#include "Car.hpp"

namespace ms
{
	Car::Car(int x, int y, float a, float b, float v)
		{
			m_a = a; m_b = b;
			m_x = x; m_y = y;
			m_v = v;
		}

	bool Car::Setup()
	{
		if(!m_texture.loadFromFile("img/car.png")) { cout << "ERROR"; return false; }
		m_shape = new Sprite();
		m_shape->setTexture(m_texture);
		m_shape->setOrigin(m_a / 2, m_b);
		m_shape->setPosition(m_x, m_y);
		return true;
	}

	Car::~Car()
		{
			delete m_shape;
		}

	   
		Sprite* Car::Get() { return m_shape; }

		void Car::Move()
		{
			m_x += m_v;
			m_shape->setPosition(m_x, m_y);
		}

		void Car::SetX(int x)
		{
			m_x = x;
			m_shape->setPosition(m_x, m_y);
		}

		int Car::GetX() { return m_x; }

		void Car::Stop() {m_v = 0;}

		void Car::Start(float v) { m_v = v; }

		void Car::Right()
		{
			m_texture.loadFromFile("img/car.png");
			m_shape->setTexture(m_texture);
		}

		void Car::Left()
		{
			m_texture.loadFromFile("img/car1.png");
			m_shape->setTexture(m_texture);
		}
}