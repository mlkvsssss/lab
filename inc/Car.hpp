#pragma once

#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <iostream>

using namespace std;
using namespace sf;

namespace ms
{
	class Car
	{
	public:

		Car(int x, int y, float a, float b, float v);
		~Car();
		bool Setup();
		Sprite* Get();
		void Move();
		void SetX(int x);
		int GetX();
		void Stop();
		void Start(float v);
		void Right();
		void Left();
		

	private:
		float m_a, m_b,m_v;
		int m_x, m_y;
		Texture m_texture;
		Sprite* m_shape;
	};
}
